package org.tsapko.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.tsapko.entity.SectorsSelection;

import java.util.List;

public interface SectorsSelectionRepository extends JpaRepository<SectorsSelection, Long> {

    SectorsSelection findByCustomerName(String name);

}
