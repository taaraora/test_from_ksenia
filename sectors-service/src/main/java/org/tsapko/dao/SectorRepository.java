package org.tsapko.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.tsapko.entity.Sector;

public interface SectorRepository extends JpaRepository<Sector, Long> {

}
