package org.tsapko.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ResponseBody;
import org.tsapko.dao.SectorsSelectionRepository;
import org.tsapko.entity.Sector;
import org.tsapko.entity.SectorsSelection;

import java.util.List;

@Service
public class SectorsSelectionService {

    @Autowired
    private SectorsSelectionRepository sectorsSelectionRepository;

    private static Logger logger = LoggerFactory.getLogger(SectorsSelectionService.class);


    @Transactional(readOnly = true)
    public List<SectorsSelection> getAllSectorsSelections() {
        return sectorsSelectionRepository.findAll();
    }

    @Transactional(readOnly = true)
    public SectorsSelection getSectorsSelectionsByName(String name) {
        return sectorsSelectionRepository.findByCustomerName(name);
    }

    @Transactional
    public @ResponseBody
    SectorsSelection saveOrUpdateSectorsSelection(SectorsSelection sectorsSelection) {
        SectorsSelection old = sectorsSelectionRepository.findByCustomerName(sectorsSelection.getCustomerName());
        if(old == null) {
            return sectorsSelectionRepository.save(sectorsSelection);
        }
        old.getSelectedSectors().clear();
        old.getSelectedSectors().addAll(sectorsSelection.getSelectedSectors());
        old.setAgreed(sectorsSelection.getAgreed());

        return sectorsSelectionRepository.save(old);
    }

}
