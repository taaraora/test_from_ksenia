package org.tsapko.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.tsapko.dao.SectorRepository;
import org.tsapko.entity.Sector;

import java.util.List;

@Service
public class SectorService {

    @Autowired
    private SectorRepository sectorRepository;

    private static Logger logger = LoggerFactory.getLogger(SectorService.class);


    @Transactional(readOnly = true)
    public List<Sector> getAllSectors() {
        return sectorRepository.findAll();
    }

}
