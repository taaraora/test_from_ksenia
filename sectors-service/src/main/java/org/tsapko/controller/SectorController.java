package org.tsapko.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.tsapko.entity.Sector;
import org.tsapko.service.SectorService;

import java.util.List;

@Controller
public class SectorController {
    private static Logger logger = LoggerFactory.getLogger(SectorController.class);

    @Autowired
    private SectorService sectorService;

    @RequestMapping(value = "/sectors",
            method = RequestMethod.GET,
            produces = "application/json")
    public @ResponseBody
    List<Sector> getSectors() {
        return sectorService.getAllSectors();
    }

}
