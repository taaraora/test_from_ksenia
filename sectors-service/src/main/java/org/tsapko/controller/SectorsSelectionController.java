package org.tsapko.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.tsapko.entity.SectorsSelection;
import org.tsapko.exception.SectorSelectionNotFoundException;
import org.tsapko.service.SectorsSelectionService;

@Controller
public class SectorsSelectionController {
    private static Logger logger = LoggerFactory.getLogger(SectorsSelectionController.class);

    @Autowired
    private SectorsSelectionService sectorsSelectionService;


    @RequestMapping(value = "/selections/{name}",
            method = RequestMethod.GET,
            produces = "application/json")
    public @ResponseBody
    SectorsSelection getSectorsSelections(@PathVariable String name) {
        SectorsSelection selection = sectorsSelectionService.getSectorsSelectionsByName(name);

        logger.warn("selection is {}", selection);
        if(selection == null) throw new SectorSelectionNotFoundException(name);
        return selection;
    }

    @RequestMapping(value = "/selections",
            method = RequestMethod.POST,
            produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    SectorsSelection postSectorsSelection(@RequestBody SectorsSelection sectorsSelection) {
        logger.warn("name: {}", sectorsSelection.getCustomerName());
        return sectorsSelectionService.saveOrUpdateSectorsSelection(sectorsSelection);
    }

}
