package org.tsapko.exception;

public class SectorSelectionNotFoundException extends RuntimeException {

    private String customerName;

    public SectorSelectionNotFoundException(String name){
        this.customerName = name;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @Override
    public String toString() {
        return "SectorSelectionNotFoundException{" +
                "customerName='" + customerName + '\'' +
                '}';
    }
}
