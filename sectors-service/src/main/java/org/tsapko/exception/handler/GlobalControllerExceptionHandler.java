package org.tsapko.exception.handler;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.tsapko.exception.SectorSelectionNotFoundException;

import javax.validation.ValidationException;
import java.sql.SQLException;

import static java.util.Collections.singletonMap;

@ControllerAdvice
public class GlobalControllerExceptionHandler extends ResponseEntityExceptionHandler {
    private final String MESSAGE = "MESSAGE";
    private final String VALIDATION_MESSAGE = "Sector validation is unsuccessful.";
    private final String SELECTIONS_NFE_MESSAGE = "Sectors not found for user - ";


    @ExceptionHandler({ SQLException.class, DataAccessException.class, DataIntegrityViolationException.class })
    public ResponseEntity<Object> anyException(RuntimeException ex, WebRequest request) {
        logger.error("Exception raised {}", ex);
        return handleExceptionInternal(ex, singletonMap(MESSAGE, "Database access exception"),
                new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);

    }

    @ExceptionHandler({ ValidationException.class })
    public ResponseEntity<Object> anyException(ValidationException ex, WebRequest request) {
        logger.error("Exception raised {}", ex);
        return handleExceptionInternal(ex, singletonMap(MESSAGE, VALIDATION_MESSAGE),
                new HttpHeaders(), HttpStatus.BAD_REQUEST, request);

    }

    @ExceptionHandler({ SectorSelectionNotFoundException.class })
    public ResponseEntity<Object> shipmentNotFoundException(SectorSelectionNotFoundException ex, WebRequest request) {
        return handleExceptionInternal(ex, singletonMap(MESSAGE, SELECTIONS_NFE_MESSAGE + ex.getCustomerName()),
                new HttpHeaders(), HttpStatus.NOT_FOUND, request);

    }


}
