package org.tsapko.entity;

import com.fasterxml.jackson.annotation.JsonIdentityReference;

import javax.persistence.*;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.*;


@Entity
public class SectorsSelection implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Size(min = 3, max = 50)
    @Column(name="customer_name")
    private String customerName;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_selections_sectors",
            joinColumns = @JoinColumn(name = "selection_id", referencedColumnName = "id",
                    foreignKey = @ForeignKey(name = "fk_user_selections_sectors_to_sectors_selection")),
            inverseJoinColumns = @JoinColumn(name = "selected_sectors_id", referencedColumnName = "id",
                    foreignKey = @ForeignKey(name = "fk_user_selections_sectors_to_sector"))
    )
    private List<Sector> selectedSectors = new ArrayList<>();


    @NotNull
    @AssertTrue
    @Column(name="agreed")
    private Boolean agreed;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }


    public Boolean getAgreed() {
        return agreed;
    }

    public void setAgreed(Boolean agreed) {
        this.agreed = agreed;
    }

    public List<Sector> getSelectedSectors() {
        return selectedSectors;
    }

    public void setSelectedSectors(List<Sector> selectedSectors) {
        this.selectedSectors = selectedSectors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        SectorsSelection that = (SectorsSelection) o;
        return Objects.equals(getCustomerName(), that.getCustomerName()) &&
                Objects.equals(getSelectedSectors(), that.getSelectedSectors()) &&
                Objects.equals(getAgreed(), that.getAgreed());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getCustomerName(), getSelectedSectors(), getAgreed());
    }

    @Override
    public String toString() {
        return "SectorsSelection{" +
                "id=" + id +
                ", customerName='" + customerName + '\'' +
                ", selectedSectors=" + selectedSectors +
                ", agreed=" + agreed +
                '}';
    }
}
