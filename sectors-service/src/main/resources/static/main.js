// сlass LvlOption represents node of nested sets structure
class LvlOption extends Option {
  constructor(text, value, left, right){
    super(text, value, false, false);
    this.dataset.right = right;
    this.dataset.left = left;
  }

  right(){
    return Number.parseInt(this.dataset.right);
  }

  left(){
    return Number.parseInt(this.dataset.left);
  }

  getLevel(){
    return Number.parseInt(this.dataset.level);
  }

  setLevel(lvl){
    this.dataset.level = lvl;
  }


  removeAllChildOptions() {
    const childOptions = this.querySelectorAll('option');
    Array.from(childOptions).forEach((op) => {
      //remove only if it is direct child
      if (op.parentNode === this) {
        this.removeChild(op);
      }
    });
  }

}

const hostname = window.location.origin;
const sectorsUri = hostname + '/sectors';
const selectionsUri = hostname + '/selections';
const form = document.querySelector('#sectors-form');
const optionsContainer = document.querySelector('#sectorsSelector');
const errorContainer = document.querySelector('#errorMessageContainer');
const userNameInput = document.querySelector('#userName');
const saveButton = document.querySelector('#saveButton');
const agreeInput = document.querySelector('#agree');
const currentSelections = sessionStorage.getItem('selections') ? JSON.parse(sessionStorage.getItem('selections')): null;

const showErr = (err) => {
  console.error('Error:', err);
  errorContainer.classList.remove('hidden');
  errorContainer.innerText = err.message;
};



const fillForm = (currentSelections) =>{
  userNameInput.value = currentSelections.customerName;
  const opsPairs = Array.from(optionsContainer.querySelectorAll('option')).map((o) => {return [Number.parseInt(o.value), o]});
  const ops = new Map(opsPairs);
  currentSelections.selectedSectors.forEach((op) => {
    const node = ops.get(op.id);
    if(node){
      node.selected = true;
    }
  });

  if(currentSelections.agreed){
    agreeInput.checked = true;
  }
};



// Name input validation message
userNameInput.addEventListener('input', (e) => {
  if (userNameInput.validity.tooShort || userNameInput.validity.valueMissing || userNameInput.validity.tooLong) {
    userNameInput.setCustomValidity('Name need to be from 2 to 50 letter length.');
    return;
  }
  if(userNameInput.validity.patternMismatch) {
    userNameInput.setCustomValidity('Name should not be blank.');
    return;
  }
  userNameInput.setCustomValidity('');
});

//saveButton
saveButton.addEventListener('click', (e) => {
  e.preventDefault();
  if(!form.checkValidity()){
    form.reportValidity();
    return;
  }
  //trim space symbols, collapse consecutive space symbols in single one
  let name = userNameInput.value.replace(/^\s+/, '').replace(/\s+$/, '').replace(/\s+/, ' ');
  userNameInput.value = name;
  let ops = Array.from(optionsContainer.selectedOptions).map((op) => {
    return {
      id: op.value,
      name: op.text,
      left: op.left(),
      right: op.right()
    };
  });
  let agreed = agreeInput.checked;

  let selections = {};
  selections.selectedSectors = [];
  selections.selectedSectors.push(...ops);
  selections.customerName = name;
  selections.agreed = agreed;
  console.log(selections);

  fetch(selectionsUri, {
    method: 'POST',
    body: JSON.stringify(selections),
    headers: new Headers({
      'Content-Type': 'application/json'
    })
  }).then((resp) => {
    if(!resp.ok){
      return showErr(new Error('error while communication with server status code' + resp.status + ', description ' + resp.statusText));
    }
    return resp.json();
  }).then((result) => {
    console.log(result);
    console.log(JSON.stringify(result));
    sessionStorage.setItem('selections', JSON.stringify(result));
  }).catch((err) => {
    showErr(err);
  });

});


// loading sectors
const sectorsData = fetch(sectorsUri).then(data => data.json()).catch((err) => {
  err.message = 'Sectors loading error: ' + err.message;
  showErr(err)
});

//building sectors hierarchy
sectorsData.then((arrayOfSectors) => {

  if (!arrayOfSectors || !arrayOfSectors.length) {
    return;
  }

  //sort array by left border
  arrayOfSectors.sort((a, b) => { return a.left - b.left; });

  const sectorsOptions = arrayOfSectors.map((e) => {
    return new LvlOption(e.name, e.id, e.left, e.right);
  });

  const temporaryContainer = document.createDocumentFragment();
  //this instance of DocumantFragment need to behave a little bit the same as LvlOption =)
  temporaryContainer.getLevel = () => {return 0};

  let parentOp = sectorsOptions[0];
  parentOp.setLevel(1);
  temporaryContainer.appendChild(parentOp);

  for (let i = 1; i < sectorsOptions.length; i++) {
    let op = sectorsOptions[i];
    let prevOp = sectorsOptions[i - 1];

    // add sibling
    if(prevOp.left() < op.left() && prevOp.right() < op.right() && op.left() - prevOp.right() === 1){
      parentOp = prevOp.parentNode;
      let lvl = parentOp.getLevel() + 1;
      op.setLevel(lvl);
      parentOp.appendChild(op);
      continue
    }

    //first node in parent
    if(prevOp.left() < op.left() && prevOp.right() > op.right()) {
      parentOp = prevOp;
      let lvl = parentOp.getLevel() + 1;
      op.setLevel(lvl);
      parentOp.appendChild(op);
      continue
    }

    //first node in new parent
    if(prevOp.left() < op.left() && prevOp.right() < op.right() && op.left() - prevOp.right() > 1) {
      let sibling = sectorsOptions.find((item) => {
        return op.left() - item.right() === 1;
      });

      parentOp = sibling.parentNode;
      let lvl = parentOp.getLevel() + 1;
      op.setLevel(lvl);
      parentOp.appendChild(op);
      continue
    }

    console.log('didn\'t added', op, 'prevNode: ', prevOp);
  }

  sectorsOptions.forEach((op) => {
    op.removeAllChildOptions();
    op.classList.add(`level-${op.getLevel()}`)
  });

  const virtualContainer = document.createDocumentFragment();
  sectorsOptions.forEach((op) => {virtualContainer.appendChild(op)});

  optionsContainer.appendChild(virtualContainer);
}).then(() => {
  if (currentSelections) {
    fillForm(currentSelections);
  }
  }
).catch((err) => {
  showErr(err);
});


