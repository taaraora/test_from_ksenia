##sectors-submission-service: 
1. Was written using Java & Spring frameworks. Because of absence requirements regarding DB, I used h2 with in-memory mode.
2. DB schema is versioned using liquibase, please check /sectors-service/src/main/resources/db/changelog/db.changelog-master.yaml
3. Binary dump & .sql script of backups is located at /db_dump folder.
4. Client part written in vanilla js using es6 features and WebAPi of modern browsers. I didn't use any build/transpiling/linting/prettifying tools for client part that's why it is not organised good.
5. To start sectors-submission-service just use command ```mvn spring-boot:run``` in /sectors-service directory. Or build docker image or download existing one using command ``` docker pull taaraora/sectors-submission-service ```. App will be accessible through port 8080.

